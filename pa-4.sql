
use Football_Assignment1;

# -------------------------- helper procedures --------------------------

# ---- IN/INOUT
# Updates the goals scored and goals conceded for a team in the standings table.
# Takes the goals scored/conceded in the match
# returns the updated total goals scored and goals conceded
DROP PROCEDURE IF EXISTS sp_update_goals_scored;
CREATE PROCEDURE sp_update_goals_scored(IN team_to_update_id INT, IN goals_scored INT, IN goals_conceded INT)
BEGIN
    UPDATE standings s
    SET s.goals_for = s.goals_for + goals_scored,
        s.goals_against = s.goals_against + goals_conceded
    WHERE s.team_id = team_to_update_id;
END;

# Update the goals scored, goals conceded and points in the FootballAssignment1.standings table
# based on the provided winner and loser ids
DROP PROCEDURE IF EXISTS sp_update_wins_losses_points;
CREATE PROCEDURE sp_update_wins_losses_points(IN winner_id INT, IN loser_id INT)
BEGIN
    UPDATE standings
    SET points = points + 3, wins = wins + 1
    WHERE team_id = winner_id;

    UPDATE standings
    SET losses = losses + 1
    WHERE team_id = loser_id;
END;


# SET GLOBAL log_bin_trust_function_creators = 1;
# Returns the points of a team from the standings table
DROP FUNCTION IF EXISTS udf_get_points;
CREATE FUNCTION udf_get_points(team_id INT) RETURNS INT
BEGIN
    DECLARE points INT DEFAULT 0;
    
    SELECT s.points INTO points
    FROM standings s
    WHERE s.team_id = team_id;
    
    RETURN points;
END;

# Returns the team_id, team_score of each of the two teams that participated in the match
DROP PROCEDURE IF EXISTS sp_get_match_info;
CREATE PROCEDURE sp_get_match_info(
    IN match_id INT, 
    OUT team1_id INT,
    OUT team2_id INT, 
    OUT team1_score INT,
    OUT team2_score INT)
BEGIN
    SELECT mp.team_id, mp.team_score
    INTO team1_id, team1_score
    FROM matches m JOIN match_participants mp ON m.away_participant_id = mp.id
    WHERE m.id = match_id
    LIMIT 1;

    SELECT mp.team_id, mp.team_score
    INTO team2_id, team2_score
    FROM matches m JOIN match_participants mp ON m.home_participant_id = mp.id
    WHERE m.id = match_id
    LIMIT 1;
END;
    

# -------------------------- main  transaction --------------------------
    
# Ends the match - calculates the winner/loser from `matches` and `match_participants` tables
# and updates the standings table
# returns the points (from standings) of each of the teams, that participated in the match
DROP PROCEDURE IF EXISTS sp_end_match;
CREATE PROCEDURE sp_end_match(IN match_id INT, OUT team1_points INT, OUT team2_points INT)
BEGIN
    DECLARE team1_id, team2_id, team1_score, team2_score INT;

    CALL sp_get_match_info(match_id, team1_id, team2_id, team1_score, team2_score);
    
    START TRANSACTION;
    
    SELECT *
    FROM standings
    WHERE team_id IN (team1_id, team2_id)
    FOR UPDATE;

    IF team1_score > team2_score THEN
        CALL sp_update_wins_losses_points(team1_id, team2_id);
    ELSEIF team1_score < team2_score THEN
        CALL sp_update_wins_losses_points(team2_id, team1_id);
    ELSE
        UPDATE standings
        SET points = points + 1, draws = draws + 1
        WHERE team_id IN (team1_id, team2_id);
    END IF;

    CALL sp_update_goals_scored(team1_id, team1_score, team2_score);
    CALL sp_update_goals_scored(team2_id, team2_score, team1_score);
    
    COMMIT;

    SET team1_points = udf_get_points(team1_id);
    SET team2_points = udf_get_points(team2_id);
END;

# -------------------------- using the procedures --------------------------

call sp_end_match(1, @team1_points, @team2_points);
select @team1_points, @team2_points;
