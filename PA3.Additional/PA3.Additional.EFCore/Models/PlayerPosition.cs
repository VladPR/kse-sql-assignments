﻿namespace PA3.Additional.EFCore.Models;

public class PlayerPosition
{
    public int Id { get; set; }
    public string PositionName { get; set; } = string.Empty;
}