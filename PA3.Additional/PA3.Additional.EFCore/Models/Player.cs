﻿namespace PA3.Additional.EFCore.Models;

public class Player
{
    public int Id { get; set; }
    public string FullName { get; set; } = string.Empty;
    public DateTime BirthDate { get; set; }
    
    public int NationalityId { get; set; }
    public Country? Nationality { get; set; }
    
    public int PositionId { get; set; }
    public PlayerPosition Position { get; set; } = null!;
}