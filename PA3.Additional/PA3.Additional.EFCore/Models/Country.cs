﻿namespace PA3.Additional.EFCore.Models;

public class Country
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}