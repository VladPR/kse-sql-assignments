﻿using Microsoft.EntityFrameworkCore;
using PA3.Additional.EFCore.Models;

namespace PA3.Additional.EFCore;

public class PlayersRepository
{
    private readonly FootballDbContext _context;
    
    public PlayersRepository(FootballDbContext context)
    {
        _context = context;
    }
    
    public async Task<IEnumerable<Player>> GetPlayersAsync()
    {
        return await _context.Players.ToListAsync();
    }
    
    public async Task<Player?> GetPlayerByIdAsync(int id)
    {
        return await _context.Players
            .Include(p => p.Nationality)
            .Include(p => p.Position)
            .FirstOrDefaultAsync(p => p.Id == id);
    }
    
    public async Task AddPlayerAsync(Player player)
    {
        await _context.Players.AddAsync(player);
        await _context.SaveChangesAsync();
    }
    
    public async Task UpdatePlayerAsync(Player updatedPlayer)
    {
        _context.Players.Update(updatedPlayer);
        await _context.SaveChangesAsync();
    }
    
    public async Task DeletePlayerAsync(int id)
    {
        var player = await _context.Players.FindAsync(id);
        if (player is null) return;
        
        _context.Players.Remove(player);
        await _context.SaveChangesAsync();
    }
}