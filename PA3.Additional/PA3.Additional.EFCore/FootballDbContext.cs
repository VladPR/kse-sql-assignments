﻿using Microsoft.EntityFrameworkCore;
using PA3.Additional.EFCore.Models;

namespace PA3.Additional.EFCore;

public class FootballDbContext: DbContext
{
    public DbSet<Player> Players { get; set; } = null!;
    public DbSet<Country> Countries { get; set; } = null!;
    public DbSet<PlayerPosition> PlayerPositions { get; set; } = null!;
    
    public FootballDbContext(DbContextOptions<FootballDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Player>()
            .HasOne(p => p.Nationality)
            .WithMany()
            .HasForeignKey(p => p.NationalityId);
        
        modelBuilder.Entity<Player>()
            .HasOne(p => p.Position)
            .WithMany()
            .HasForeignKey(p => p.PositionId);
    }
}