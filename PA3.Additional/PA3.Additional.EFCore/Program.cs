﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MySqlConnector;
using PA3.Additional.EFCore;
using PA3.Additional.EFCore.Models;

var configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
builder.Services.AddDbContext<FootballDbContext>( options =>
{
    options.UseMySQL(configuration.GetConnectionString("MySql")!)
        .UseSnakeCaseNamingConvention();
});
 
builder.Services.AddTransient<MySqlConnection>(_ => new MySqlConnection(
    configuration.GetConnectionString("MySql")));

builder.Services.AddScoped<PlayersLinqRepository>();
builder.Services.AddScoped<PlayersTransactionsRepository>();

var host = builder.Build();

using (var scope = host.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    /*var repository = services.GetRequiredService<PlayersLinqRepository>();
    var players = await repository.GetPlayersRawAsync();
    
    foreach (var player in players)
    {
        Console.WriteLine(player.FullName);
    }
    
    Player? playerById = await repository.GetPlayerByIdAsync(1, includeRelated: true);
    Console.WriteLine($"{playerById?.FullName}, {playerById?.Nationality?.Name}, {playerById?.Position.PositionName}");*/
    
    var transactionRepository = services.GetRequiredService<PlayersTransactionsRepository>();
    int playerId = await transactionRepository.AddPlayerAsync(new Player
    {
        FullName = "John SQL Doe",
        BirthDate = new DateTime(1990, 1, 1),
        NationalityId = 1,
        PositionId = 1
    });

    Console.WriteLine($"Player ID: {playerId}");
    
    await transactionRepository.DeleteAsync(playerId);
    Console.WriteLine("Player deleted.");
}

host.Run();

