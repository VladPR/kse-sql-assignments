﻿using System.Data;
using Dapper;
using MySqlConnector;
using PA3.Additional.EFCore.Models;

namespace PA3.Additional.EFCore;

public class PlayersTransactionsRepository
{
    private readonly MySqlConnection _connection;

    public PlayersTransactionsRepository(MySqlConnection connection)
    {
        _connection = connection;
    }
    
    public async Task<int> AddPlayerAsync(Player player)
    {
        if (_connection.State != ConnectionState.Open)
            await _connection.OpenAsync();
        
        await using var transaction = await _connection.BeginTransactionAsync();
        try
        {
            const string sql = 
                """
                INSERT INTO players (full_name, birth_date, nationality_id, position_id)
                VALUES (@FullName, @BirthDate, @NationalityId, @PositionId);
                """;
            
            await _connection.ExecuteAsync(sql, player, transaction);
            
            int playerId = await _connection.ExecuteScalarAsync<int>(
                "SELECT LAST_INSERT_ID();", transaction: transaction);

            await transaction.CommitAsync();
            
            return playerId;
        }
        catch (Exception)
        {
            await transaction.RollbackAsync();
            throw;
        }
    }

    public async Task DeleteAsync(int playerId)
    {
        if (_connection.State != ConnectionState.Open)
            await _connection.OpenAsync();
        
        await using var transaction = await _connection.BeginTransactionAsync();
        const string selectSql = "SELECT * FROM players WHERE id = @PlayerId;";
        
        try
        {
            var player = _connection.QuerySingleOrDefault<Player>(
                selectSql, new { PlayerId = playerId }, transaction);
            
            if (player is null)
                return;
            
            const string deleteSql = "DELETE FROM players WHERE id = @PlayerId;";
            await _connection.ExecuteAsync(deleteSql, new { PlayerId = playerId }, transaction);

            await transaction.CommitAsync();
        }
        catch (Exception)
        {
            await transaction.RollbackAsync();
            throw;
        }
    }
}