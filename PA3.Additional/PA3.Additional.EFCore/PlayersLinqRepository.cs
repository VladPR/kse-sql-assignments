﻿using Microsoft.EntityFrameworkCore;
using PA3.Additional.EFCore.Models;

namespace PA3.Additional.EFCore;

public class PlayersLinqRepository
{
    private readonly FootballDbContext _context;

    public PlayersLinqRepository(FootballDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Player>> GetPlayersAsync()
    {
        var linq = from p in _context.Players select p;
        return await linq.ToListAsync();
    }
    
    public async Task<IEnumerable<Player>> GetPlayersRawAsync()
    {
        return await _context.Players.FromSqlRaw("SELECT * FROM players").ToListAsync();
    }
    
    public async Task<Player?> GetPlayerByIdAsync(int id, bool includeRelated = false)
    {
        if (includeRelated)
        {
           return await (
                   from p in _context.Players 
                   join country in _context.Countries on p.NationalityId equals country.Id 
                   join position in _context.PlayerPositions on p.PositionId equals position.Id 
                   where p.Id == id 
                   select new Player 
                   {
                       Id = p.Id,
                       FullName = p.FullName,
                       BirthDate = p.BirthDate,
                       NationalityId = p.NationalityId,
                       Nationality = country,
                       PositionId = p.PositionId,
                       Position = position 
                   })
               .FirstOrDefaultAsync();
            
        }
        
        return await (
            from p in _context.Players 
            where p.Id == id 
            select p).FirstOrDefaultAsync();
    }
    
    public async Task UpdatePlayerAsync(Player updatedPlayer)
    {
        Player? player = await GetPlayerByIdAsync(updatedPlayer.Id);
        
        if (player is null) return;
        
        player.FullName = updatedPlayer.FullName;
        player.BirthDate = updatedPlayer.BirthDate;
        player.NationalityId = updatedPlayer.NationalityId;
        player.PositionId = updatedPlayer.PositionId;
        
        await _context.SaveChangesAsync();
    }
    
    public async Task DeletePlayerAsync(int id)
    { 
        Player? player = await GetPlayerByIdAsync(id);
        if (player is null) return;
        
        _context.Players.Remove(player);
        await _context.SaveChangesAsync();
    }
}