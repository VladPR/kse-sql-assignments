USE Football_Assignment1;

# ------------------- 1. = with non-correlated subqueries result -------------------

# SELECT - all cities in Belgium
SELECT * 
FROM cities 
WHERE country_id = (SELECT countries.id FROM countries WHERE countries.name = 'Belgium');

# UPDATE - all players that are 'Forward' to 'Right winger'
UPDATE players
SET position_id = (SELECT id 
                   FROM player_positions 
                   WHERE position_name = 'Right winger')
WHERE position_id = (SELECT id 
                     FROM player_positions 
                     WHERE position_name = 'Forward');

# Delete - all goals scored by 'Forward'
DELETE FROM goals
WHERE player_id = (SELECT id 
                   FROM players 
                   WHERE position_id = (SELECT id 
                                         FROM player_positions 
                                         WHERE position_name = 'Forward'));


# ------------------- 2. IN with non-correlated subqueries result -------------------

# SELECT - all stadiums in Belgium
SELECT *
FROM stadiums
WHERE city_id IN (SELECT cities.id 
                  FROM cities JOIN countries on countries.id = cities.country_id
                  WHERE countries.name = 'Belgium');

# UPDATE - all 
    
# DELETE - all goals scored by 'Right winger' or 'Left winger'
DELETE FROM goals
WHERE player_id IN (SELECT id 
                    FROM players 
                    WHERE position_id IN (SELECT id 
                                          FROM player_positions 
                                          WHERE position_name = 'Right winger' 
                                          OR position_name = 'Left winger'));

# ------------------- 3. NOT IN with non-correlated subqueries result -------------------

# SELECT - all players that are not 'Centre-back'
SELECT *
FROM players
WHERE position_id NOT IN (SELECT id 
                          FROM player_positions 
                          WHERE position_name = 'Centre-back');

# UPDATE - all players that are not 'Centre-back' to 'Defensive midfielder'
UPDATE players
SET position_id = (SELECT id 
                   FROM player_positions 
                   WHERE position_name = 'Defensive midfielder')
WHERE position_id NOT IN (SELECT id 
                          FROM player_positions 
                          WHERE position_name = 'Centre-back');

# DELETE - all goals scored by players that are not 'Centre-back'
DELETE FROM goals
WHERE player_id NOT IN (SELECT id 
                        FROM players 
                        WHERE position_id = (SELECT id 
                                              FROM player_positions 
                                              WHERE position_name = 'Centre-back'));


# ------------------- 4. EXISTS with non-correlated subqueries result -------------------
    
-- does not make any sense to use EXISTS with non-correlated subqueries,
-- since the result will be fully dependent on the result of the subquery.
-- it might be used as an IF statement, but it is not the case in this assignment
    
# ------------------- 5. NOT EXISTS with non-correlated subqueries result -------------------
    
-- does not make any sense to use NOT EXISTS with non-correlated subqueries,
-- since the result will be fully dependent on the result of the subquery.
-- it might be used as an IF NOT statement, but it is not the case in this assignment

# ------------------- 6. = with correlated subqueries result -------------------

# SELECT - all distinct number of points from standings, when exactly 2 teams have the same number of points
SELECT DISTINCT points
FROM standings s1
WHERE 1 = (SELECT COUNT(*)
           FROM standings s2 
           WHERE s1.points = s2.points 
           AND s1.team_id != s2.team_id);

# UPDATE - all standings that have the same number of points as 'Paris Saint-Germain' to 100
UPDATE standings s1
SET points = 100
WHERE 1 = (SELECT COUNT(*)
           FROM standings s2 
           WHERE s2.team_id = (SELECT id 
                               FROM teams 
                               WHERE name = 'Paris Saint-Germain')
           AND s1.points = s2.points
           AND s1.team_id != s2.team_id);

# DELETE - all goals scored by players that are from the same country as 'Virgil van Dijk'
DELETE FROM goals
WHERE player_id IN (SELECT id 
                    FROM players p1
                    WHERE 1 = (SELECT COUNT(*)
                               FROM players p2
                               WHERE p2.full_name = 'Virgil van Dijk'
                               AND p1.nationality_id = p2.nationality_id
                               AND p1.id != p2.id));


# ------------------- 7. IN with correlated subqueries result -------------------

# SELECT - all countries that have a city
SELECT *
FROM countries 
WHERE id IN (SELECT country_id 
             FROM cities);

# UPDATE - all countries that have a stadium to 'Europe'
UPDATE countries
SET name = 'Europe'
WHERE id IN (SELECT country_id 
             FROM cities
             WHERE id IN (SELECT city_id 
                          FROM stadiums));

# DELETE - all goals scored by players that are from Belgium
DELETE FROM goals
WHERE player_id IN (SELECT id 
                    FROM players 
                    WHERE nationality_id = (SELECT id 
                                             FROM countries 
                                             WHERE name = 'Belgium'));

# ------------------- 8. NOT IN with correlated subqueries result -------------------
    
# SELECT - all players that have not scored a goal
SELECT *
FROM players
WHERE id NOT IN (SELECT player_id 
                 FROM goals);

# UPDATE - all players that have not scored a goal to 'Defensive midfielder'
UPDATE players
SET position_id = (SELECT id 
                   FROM player_positions 
                   WHERE position_name = 'Defensive midfielder')
WHERE id NOT IN (SELECT player_id 
                 FROM goals);

# DELETE - all goals scored by players that have not scored a goal :)
DELETE FROM goals
WHERE player_id NOT IN (SELECT id 
                        FROM players 
                        WHERE id NOT IN (SELECT player_id 
                                         FROM goals));

# ------------------- 9. EXISTS with correlated subqueries result -------------------

# SELECT - all players that have scored a goal
SELECT *
FROM players
WHERE EXISTS (SELECT * 
              FROM goals 
              WHERE goals.player_id = players.id);

# UPDATE - all players that have scored a goal to 'Forward'
UPDATE players
SET position_id = (SELECT id 
                   FROM player_positions 
                   WHERE position_name = 'Forward')
WHERE EXISTS (SELECT * 
              FROM goals 
              WHERE goals.player_id = players.id);

# DELETE - all goals scored by players that have scored a goal :)
DELETE FROM goals
WHERE EXISTS (SELECT * 
              FROM players 
              WHERE players.id = goals.player_id);

# ------------------- 10. NOT EXISTS with correlated subqueries result -------------------

# SELECT - all cities that do not have a stadium
SELECT *
FROM cities
WHERE NOT EXISTS (SELECT * 
                  FROM stadiums 
                  WHERE stadiums.city_id = cities.id);

# UPDATE - all cities that do not have a stadium to 'USA State'
UPDATE cities
SET name = 'USA State'
WHERE NOT EXISTS (SELECT * 
                  FROM stadiums 
                  WHERE stadiums.city_id = cities.id);

# DELETE - all goals scored by players whose country does not have a stadium
# DELETE FROM goals
SELECT *
FROM goals
WHERE NOT EXISTS (SELECT * 
                  FROM stadiums 
                  WHERE stadiums.city_id IN (SELECT cities.id 
                                             FROM cities 
                                             WHERE cities.country_id = (SELECT players.nationality_id
                                                                        FROM players 
                                                                        WHERE players.id = goals.player_id)));

# -------------- four SELECT queries that include the clause UNION / UNION ALL / INTERSECT / EXCEPT ------------

# SELECT (UNION) - all players that are 'Forward' or 'Right winger'
SELECT *
FROM players
WHERE position_id = (SELECT id 
                     FROM player_positions 
                     WHERE position_name = 'Forward')
UNION
SELECT *
FROM players
WHERE position_id = (SELECT id 
                     FROM player_positions 
                     WHERE position_name = 'Right winger');

# SELECT (UNION ALL) - all players that are in team 'Paris Saint-Germain' or 'Ajax' (since player can be in >1 team)
SELECT *
FROM players
WHERE id IN (SELECT player_id 
             FROM players_teams 
             WHERE team_id = (SELECT id 
                              FROM teams 
                              WHERE name = 'Paris Saint-Germain'))
UNION ALL
SELECT *
FROM players
WHERE id IN (SELECT player_id 
             FROM players_teams 
             WHERE team_id = (SELECT id 
                              FROM teams 
                              WHERE name = 'Ajax'));

# SELECT (INTERSECT) - all matches that were at 'Parc des Princes' and between 'Paris Saint-Germain' and 'Ajax'
SELECT *
FROM matches
WHERE stadium_id = (SELECT id 
                    FROM stadiums 
                    WHERE name = 'Parc des Princes')
INTERSECT
SELECT *
FROM matches
WHERE home_participant_id = (SELECT id 
                             FROM teams 
                             WHERE name = 'Paris Saint-Germain')
AND away_participant_id = (SELECT id
                            FROM teams 
                            WHERE name = 'Ajax');

# SELECT (EXCEPT) - all players that are from France but not in 'Paris Saint-Germain' team
SELECT *
FROM players
WHERE nationality_id = (SELECT id 
                        FROM countries 
                        WHERE name = 'France')
EXCEPT
SELECT *
FROM players
WHERE id IN (SELECT player_id 
             FROM players_teams 
             WHERE team_id = (SELECT id 
                              FROM teams 
                              WHERE name = 'Paris Saint-Germain'));

