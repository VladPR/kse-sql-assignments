# ---------------------------- creating database tables -----------------------------

create database Football_Assignment1;
use Football_Assignment1;

CREATE TABLE countries
(
    id int PRIMARY KEY auto_increment,
    name varchar(50) not null unique check (name != '')
);

CREATE TABLE cities
(
    id int PRIMARY KEY auto_increment,
    name varchar(50) not null unique check (name != ''),
    country_id int not null,
    FOREIGN KEY (country_id) REFERENCES countries(id)
);

CREATE TABLE leagues
(
    id int PRIMARY KEY auto_increment,
    name varchar(50) not null unique check (name != ''),
    country_id int not null,
    FOREIGN KEY (country_id) REFERENCES countries(id)
);

CREATE TABLE stadiums
(
    id int PRIMARY KEY auto_increment,
    name varchar(50) not null unique check (name != ''),
    city_id int not null,
    capacity int not null check (capacity > 0),
    FOREIGN KEY (city_id) REFERENCES cities(id)
);

CREATE TABLE team_managers
(
    id int PRIMARY KEY auto_increment,
    full_name varchar(50) not null check (full_name != '')
);

CREATE TABLE player_positions
(
    id int PRIMARY KEY auto_increment,
    position_name varchar(50) not null unique check (position_name != '')
);

CREATE TABLE teams
(
    id int PRIMARY KEY auto_increment,
    name varchar(50) not null check (name != ''),
    home_stadium_id int not null,
    manager_id int not null,
    league_id int not null,
    FOREIGN KEY (home_stadium_id) REFERENCES stadiums(id),
    FOREIGN KEY (manager_id) REFERENCES team_managers (id),
    FOREIGN KEY (league_id) REFERENCES leagues(id)
);

CREATE TABLE players
(
    id int PRIMARY KEY auto_increment,
    full_name varchar(50) not null check (full_name != ''),
    birth_date date,
    nationality_id int,
    position_id int not null,
    FOREIGN KEY (nationality_id) REFERENCES countries(id),
    FOREIGN KEY (position_id) REFERENCES player_positions(id)
);

CREATE TABLE players_teams
(
    player_id int not null,
    team_id int not null,
    PRIMARY KEY (player_id, team_id),
    FOREIGN KEY (player_id) REFERENCES players(id),
    FOREIGN KEY (team_id) REFERENCES teams (id)
);

CREATE TABLE match_participants
(
    id int PRIMARY KEY auto_increment,
    team_id int not null,
    team_score int not null check (team_score >= 0),
    FOREIGN KEY (team_id) REFERENCES teams (id)
);

CREATE TABLE matches
(
    id int PRIMARY KEY auto_increment,
    home_participant_id int not null,
    away_participant_id int not null,
    date_time datetime not null,
    stadium_id int not null,
    FOREIGN KEY (home_participant_id) REFERENCES match_participants (id),
    FOREIGN KEY (away_participant_id) REFERENCES match_participants (id),
    FOREIGN KEY (stadium_id) REFERENCES stadiums(id)
);

CREATE TABLE goals
(
    id int PRIMARY KEY auto_increment,
    match_id int not null,
    player_id int not null,
    team_id int not null,
    time int not null,
    FOREIGN KEY (match_id) REFERENCES matches (id),
    FOREIGN KEY (player_id) REFERENCES players(id),
    FOREIGN KEY (team_id) REFERENCES teams(id)
);

CREATE TABLE standings
(
    id int PRIMARY KEY auto_increment,
    team_id int not null,
    points int not null,
    wins int not null,
    draws int not null,
    losses int not null,
    goals_for int not null,
    goals_against int not null,
    FOREIGN KEY (team_id) REFERENCES teams (id)
);



# ---------------------------- triggers -----------------------------

# updates the Matches table score column after a goal is inserted
    
# create trigger Trigger_Goals_AfterInsert
#     after insert on Goals
#     for each row
#     begin
#         update Matches
#         SET HomeTeamScore = HomeTeamScore + IF(HomeTeamId = new.TeamId, 1, 0),
#             AwayTeamScore = AwayTeamScore + IF(AwayTeamId = new.TeamId, 1, 0)
#         WHERE Id = new.MatchId;
#     end;

# ---------------------------- inserting data --------------------------

use Football_Assignment1;

INSERT INTO countries (name)
VALUES
    ('France'),
    ('Brazil'),
    ('Argentina'),
    ('Netherlands'),
    ('Portugal'),
    ('Belgium'),
    ('Mexico'),
    ('Turkey'),
    ('Greece'),
    ('Russia'),
    ('Sweden'),
    ('Denmark'),
    ('Norway'),
    ('Japan'),
    ('South Korea');

INSERT INTO cities (name, country_id)
VALUES
    ('Paris', 13),
    ('Rio de Janeiro', 2),
    ('Buenos Aires', 3),
    ('Amsterdam', 4),
    ('Lisbon', 5),
    ('Brussels', 6),
    ('Mexico City', 7),
    ('Istanbul', 8),
    ('Athens', 9),
    ('Moscow', 10),
    ('Stockholm', 11),
    ('Copenhagen', 12),
    ('Oslo', 13),
    ('Tokyo', 14),
    ('Seoul', 15);

INSERT INTO leagues (name, country_id)
VALUES
    ('Ligue 1', 13),
    ('Serie A', 5),
    ('Brasileirão Série A', 2),
    ('Primera División', 3),
    ('Eredivisie', 4),
    ('Primeira Liga', 5),
    ('Jupiler Pro League', 6),
    ('Liga MX', 7),
    ('Süper Lig', 8),
    ('Super League Greece', 9),
    ('Russian Premier League', 10),
    ('Allsvenskan', 11),
    ('Superligaen', 12),
    ('Eliteserien', 13),
    ('J1 League', 14),
    ('K League 1', 15);

INSERT INTO stadiums (name, city_id, capacity)
VALUES
    ('Parc des Princes', 1, 48678),
    ('Maracanã', 2, 78838),
    ('Estadio Monumental', 3, 67000),
    ('Johan Cruyff Arena', 4, 54814),
    ('Estádio da Luz', 5, 64947),
    ('King Baudouin Stadium', 6, 50000),
    ('Estadio Azteca', 7, 87187),
    ('Şükrü Saracoğlu Stadium', 8, 50302),
    ('Olympic Stadium', 9, 69728),
    ('Luzhniki Stadium', 10, 81000),
    ('Friends Arena', 11, 50000),
    ('Parken Stadium', 12, 38065),
    ('Ullevaal Stadion', 13, 28000),
    ('National Stadium', 14, 61500),
    ('Seoul World Cup Stadium', 15, 66704);

INSERT INTO team_managers (full_name)
VALUES
    ('Thomas Tuchel'),
    ('Diego Simeone'),
    ('Carlo Ancelotti'),
    ('Marcelo Bielsa'),
    ('José Mourinho'),
    ('Roberto Martinez'),
    ('Tata Martino'),
    ('Fatih Terim'),
    ('Fernando Santos'),
    ('Stanislav Cherchesov'),
    ('Janne Andersson'),
    ('Kasper Hjulmand'),
    ('Ståle Solbakken'),
    ('Hajime Moriyasu'),
    ('Paulo Bento');

INSERT INTO player_positions (position_name)
VALUES
    ('Left-back'),
    ('Right-back'),
    ('Centre-back'),
    ('Defensive midfielder'),
    ('Central midfielder'),
    ('Attacking midfielder'),
    ('Left winger'),
    ('Right winger'),
    ('Forward');

INSERT INTO teams (name, home_stadium_id, manager_id, league_id)
VALUES
    ('Paris Saint-Germain', 1, 1, 16),
    ('Flamengo', 2, 3, 3),
    ('Boca Juniors', 3, 4, 4),
    ('Ajax', 4, 5, 5),
    ('Benfica', 5, 6, 6),
    ('Club Brugge', 6, 7, 7),
    ('Club América', 7, 8, 8),
    ('Galatasaray', 8, 9, 9),
    ('Olympiacos', 9, 10, 10),
    ('Spartak Moscow', 10, 11, 11),
    ('AIK', 11, 12, 12),
    ('FC Copenhagen', 12, 13, 13),
    ('Rosenborg', 13, 14, 14),
    ('FC Tokyo', 14, 15, 15);

INSERT INTO players (full_name, birth_date, nationality_id, position_id)
VALUES
    ('Neymar Jr.', '1992-02-05', 2, 8),
    ('Lionel Messi', '1987-06-24', 3, 9),
    ('Virgil van Dijk', '1991-07-08', 4, 3),
    ('Mohamed Salah', '1992-06-15', 10, 8),
    ('Eden Hazard', '1991-01-07', 6, 7),
    ('Andrea Pirlo', '1979-05-19', 4, 5),
    ('Vinícius Júnior', '2000-07-12', 2, 8),
    ('Joaquín Correa', '1994-08-13', 3, 9),
    ('Matthijs de Ligt', '1999-08-12', 4, 3),
    ('Joaõ Félix', '1999-11-10', 5, 9),
    ('Kevin De Bruyne', '1991-06-28', 6, 5),
    ('Raheem Sterling', '1994-12-08', 1, 8),
    ('Sadio Mané', '1992-04-10', 12, 8),
    ('Kasper Dolberg', '1997-10-06', 13, 9),
    ('Takefusa Kubo', '2001-06-04', 14, 8);

INSERT INTO players_teams (player_id, team_id)
VALUES
    (1, 1),
    (2, 1),
    (3, 2),
    (4, 2),
    (5, 3),
    (6, 3),
    (7, 4),
    (8, 4),
    (9, 5),
    (10, 5),
    (11, 6),
    (12, 6),
    (13, 7),
    (14, 7),
    (15, 8);

INSERT INTO match_participants (team_id, team_score)
VALUES
    (1, 3),
    (2, 1),
    (3, 2),
    (4, 1),
    (5, 2),
    (6, 2),
    (7, 1),
    (8, 0),
    (9, 3),
    (10, 2),
    (11, 1),
    (12, 1),
    (13, 2),
    (14, 0),
    (6, 0),
    (5, 0);

INSERT INTO matches (home_participant_id, away_participant_id, date_time, stadium_id)
VALUES
    (1, 2, '2024-02-18 20:00:00', 1),
    (3, 4, '2024-02-19 19:45:00', 3),
    (5, 6, '2024-02-20 19:30:00', 5),
    (7, 8, '2024-02-21 20:00:00', 7),
    (9, 10, '2024-02-22 19:00:00', 9),
    (11, 12, '2024-02-23 20:15:00', 11),
    (13, 14, '2024-02-24 20:30:00', 13),
    (6, 5, '2024-02-25 19:45:00', 7);

INSERT INTO goals (match_id, player_id, team_id, time)
VALUES
    (1, 1, 1, 15),
    (1, 2, 1, 30),
    (2, 3, 2, 25),
    (2, 4, 2, 20),
    (3, 5, 3, 40),
    (3, 6, 3, 60),
    (4, 7, 4, 10),
    (4, 8, 4, 75),
    (5, 9, 5, 5),
    (5, 10, 5, 85),
    (6, 11, 6, 20),
    (6, 12, 6, 55),
    (7, 13, 7, 30),
    (7, 14, 7, 45),
    (8, 15, 8, 25),
    (8, 6, 8, 80),
    (1, 7, 9, 15),
    (1, 8, 9, 50),
    (2, 9, 10, 35),
    (2, 9, 10, 70),
    (3, 1, 11, 40),
    (3, 2, 11, 65),
    (4, 3, 12, 20),
    (4, 4, 12, 75),
    (5, 5, 13, 10),
    (5, 6, 13, 60),
    (6, 7, 14, 30),
    (6, 8, 14, 45),
    (7, 9, 11, 25),
    (7, 9, 1, 80),
    (8, 1, 10, 15),
    (8, 2, 10, 30),
    (1, 3, 1, 40),
    (1, 4, 1, 60),
    (2, 5, 2, 10),
    (2, 6, 2, 85),
    (3, 7, 3, 20),
    (3, 8, 3, 55),
    (4, 9, 4, 30),
    (4, 10, 4, 45),
    (5, 11, 5, 25),
    (5, 12, 5, 80),
    (6, 13, 6, 15),
    (6, 14, 6, 50),
    (7, 15, 7, 35),
    (7, 6, 7, 70),
    (8, 7, 8, 40),
    (8, 8, 8, 65);

INSERT INTO standings (team_id, points, wins, draws, losses, goals_for, goals_against)
VALUES
    (1, 6, 2, 0, 0, 5, 1),
    (2, 6, 2, 0, 0, 3, 2),
    (3, 6, 2, 0, 0, 4, 3),
    (4, 6, 2, 0, 0, 3, 1),
    (5, 6, 2, 0, 0, 5, 2),
    (6, 4, 1, 1, 0, 4, 3),
    (7, 4, 1, 1, 0, 3, 1),
    (8, 4, 1, 1, 0, 2, 0),
    (9, 4, 1, 1, 0, 5, 4),
    (10, 3, 1, 0, 1, 5, 5),
    (11, 3, 0, 1, 1, 2, 2),
    (12, 1, 0, 0, 2, 1, 2),
    (13, 0, 0, 0, 2, 0, 4),
    (14, 0, 0, 0, 2, 0, 3);

# ------------------------------ queries -----------------------------

use Football_Assignment1;

# 1. Teams with average number of goals 
# scored per match in the a specified league.

CREATE PROCEDURE SP_TopTeams_HighestAvgGoalsScoredPerMatch 
    (IN leagueName varchar(50))
BEGIN 
    SELECT 
        t.Name as team_name,
        avg(s.goals_for / (s.Wins + s.Draws + s.Losses)) as avg_goals_scored_per_match
    FROM teams t 
        JOIN standings s ON t.Id = s.team_id
        JOIN (SELECT Id FROM leagues l WHERE l.Name = leagueName) AS league 
            ON t.league_id = league.Id
    WHERE 
        (s.Wins + s.Draws + s.Losses) > 0
    GROUP BY t.Id
    ORDER BY avg_goals_scored_per_match DESC;
END;

# 2: Players who have scored more than N goals in matches 
# played at stadiums with a capacity greater than S seats

CREATE PROCEDURE SP_Players_ScoredMoreThanGoals_AtStadiumsWithGreaterCapacity
    (IN goals int, IN capacity int)
BEGIN 
    SELECT 
        p.full_name as player_name, count(*) as goals_scored
    FROM players p 
        JOIN goals g ON p.Id = g.player_id
        JOIN matches m ON g.match_id = m.Id
        JOIN stadiums s ON m.stadium_id = s.Id
    WHERE s.Capacity > capacity
    GROUP BY p.Id
    HAVING goals_scored > goals
    ORDER BY goals_scored DESC ;
END;

# 3: Top N teams with the highest number of points 
# in the top 3 leagues (by the number of points)

CREATE PROCEDURE SP_TopTeams_HighestPoints_Top3Leagues 
    (IN topN int)
BEGIN
    SELECT
        t.Name as team_name, top3_leagues.Name as league_name, s.Points
    FROM standings s
             JOIN teams t ON s.team_id = t.Id
             JOIN (SELECT l.Id, l.Name
                   FROM leagues l
                       JOIN teams t ON l.Id = t.league_id
                       JOIN standings s ON t.Id = s.team_id
                   GROUP BY l.Id
                   ORDER BY sum(s.Points) DESC
                   LIMIT 3) AS top3_leagues
                 ON t.league_id = top3_leagues.Id
    ORDER BY s.Points DESC 
    LIMIT topN;
END;

# ------------------------------ testing -----------------------------

# call SP_TopTeams_HighestAvgGoalsScoredPerMatch('K League 1');
# call SP_Players_ScoredMoreThanGoals_AtStadiumsWithGreaterCapacity(2, 50000);
# call SP_TopTeams_HighestPoints_Top3Leagues(3);
    
# ------------------------------ modifying the schema -----------------------------
USE Football_Assignment1;

ALTER TABLE goals
    ADD COLUMN match_participant_id INT,
    ADD CONSTRAINT fk_goals_match_participant
        FOREIGN KEY (match_participant_id)
            REFERENCES match_participants(id);

UPDATE goals AS g
    JOIN match_participants AS mp ON g.team_id = mp.team_id
SET g.match_participant_id = mp.id;

ALTER TABLE goals DROP CONSTRAINT goals_ibfk_3;
ALTER TABLE goals DROP COLUMN team_id;

# ------------------------------ two new queries ----------------------------

# countries by the most successful teams
SELECT
    c.name as country_name,
    t.name as team_name,
    s.points
FROM countries c
    JOIN cities ci ON c.id = ci.country_id
    JOIN stadiums st ON ci.id = st.city_id
    JOIN teams t ON st.id = t.home_stadium_id
    JOIN standings s ON t.id = s.team_id
ORDER BY s.points DESC;

# teams by highest goal difference
SELECT
    t.id as team_id,
    t.name as team_name,
    (s.goals_for - s.goals_against) as goal_difference
FROM teams t
    JOIN standings s ON t.id = s.team_id
ORDER BY goal_difference DESC;

# ------------------------------ show indexes boost -----------------------------

# USE Football_Assignment1;
# CREATE TABLE team_managers_indexed like team_managers;
# INSERT INTO team_managers_indexed SELECT * FROM team_managers;
# ALTER TABLE team_managers_indexed ADD INDEX (full_name);

SELECT * FROM team_managers_indexed WHERE full_name = 'Thomas Tuchel';
SELECT * FROM team_managers WHERE full_name = 'Thomas Tuchel';


